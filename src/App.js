import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import Dashboard from './components/Dashboard/Dashboard';
import LoginPage from './pages/LoginPage/LoginPage';
import * as frontUrls from './constants/frontUrls';
import ZoneEconomiquePage from './pages/zoneEconomiquePage/ZoneEconomiquePage';
import FlexColumn from './components/FlexColumn';

function App() {
  return (
    <BrowserRouter>
            <Routes>
              <Route>
                <Route path='/' element={<LoginPage />} />
                <Route path={frontUrls.dashboardUrl} element={<Dashboard />  }/>
                <Route path={frontUrls.zoneEconomiqueListUrl} element={<ZoneEconomiquePage />} />
              </Route>
            </Routes>
    </BrowserRouter >
  );
}

export default App;
