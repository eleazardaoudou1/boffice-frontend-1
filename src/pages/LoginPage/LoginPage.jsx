import React, { useEffect, useState } from 'react';
import './loginStyle.css';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { apiLogin } from '../../constants/backUrls';
import { dashboardUrl } from '../../constants/frontUrls';

const LoginPage = () => {


	const navigate = useNavigate();

	const [data, setData] = useState();

	/**
	 * gérer la connexion
	 * @param 
	 */
	const handleSubmit = (e) => {
		e.preventDefault();
		axios.post(apiLogin, data)
			.then((result) => {
				if (result.status == 200) {
					localStorage.setItem('token', result.data.token)
					navigate(dashboardUrl)
				}
			})
			.catch((error) => {console.log(error) })
	}

	return (

		<div>
			<div className="d-flex flex-column flex-root">
				<div className="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" >
					<div className="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
						<a href="../../demo20/dist/index.html" className="mb-12">
							<img alt="Logo" src="assets/media/logos/logo-1.svg" className="h-40px" />
						</a>
						<div className="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
							<form className="form w-100" novalidate="novalidate" id="kt_sign_in_form" >
								<div className="text-center mb-10">
									<h1 className="text-dark mb-3">Connexion</h1>
								</div>
								<div className="fv-row mb-10">
									<label className="form-label fs-6 fw-bolder text-dark">Email ou nom d'utilisateur</label>
									<input className="form-control form-control-lg form-control-solid" type="text" name="email" autocomplete="off" onChange={(e) => { setData({ ...data, username: e.target.value }) }} />
								</div>

								<div className="fv-row mb-10">
									<div className="d-flex flex-stack mb-2">
										<label className="form-label fw-bolder text-dark fs-6 mb-0">Mot de passe</label>

										<a href="../../demo20/dist/authentication/layouts/basic/password-reset.html" className="link-primary fs-6 fw-bolder">Mot de passe oublié ?</a>
									</div>
									<input className="form-control form-control-lg form-control-solid" type="password" name="password" autocomplete="off" onChange={(e) => { setData({ ...data, password: e.target.value }) }} />
								</div>

								<div className="text-center">
									<button type="submit" id="kt_sign_in_submit" className="btn btn-lg btn-primary w-100 mb-5" onClick={(e) => handleSubmit(e)} >
										<span className="indicator-label">Continuer</span>
										<span className="indicator-progress">Please wait...
											<span className="spinner-border spinner-border-sm align-middle ms-2"></span></span>
									</button>
								</div>
							</form>
						</div>
					</div>

					<div className="d-flex flex-center flex-column-auto p-10">
						<div className="d-flex align-items-center fw-bold fs-6">
							<a href="https://keenthemes.com" className="text-muted text-hover-primary px-2">About</a>
							<a href="mailto:support@keenthemes.com" className="text-muted text-hover-primary px-2">Contact</a>
							<a href="https://1.envato.market/EA4JP" className="text-muted text-hover-primary px-2">Contact Us</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default LoginPage;