export const baseURL = "http://localhost:8080/api/v1";
export const apiLogin =  baseURL + "/auth/login";
export const getAllZoneEconomiquesUrl = baseURL + "/zones-economique";
export const addZoneEconomiqueUrl = baseURL + "/zones-economique";