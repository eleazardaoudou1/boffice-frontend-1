import axios from "axios";
import { addZoneEconomiqueUrl, baseURL, getAllZoneEconomiquesUrl } from "../constants/backUrls";


const api = axios.create({
  baseURL: baseURL,
  // timeout: 3600,
  headers: {
    Accept: "application/json",
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'X-Content-Type-Options': "nosniff",
    "X-XSS-Protection": "1; mode=block"
  }
});


/**
 * Get all zones economiques
 * @returns 
 */
export function getAllZoneEconomiques(token) {
  const header = {
    Authorization: `Bearer ${token}`,
  };
  const response = axios.get(getAllZoneEconomiquesUrl, { headers: header });
  return response.then(
    (data) => data)
    //console.log(data))
    .catch((error) => error);
}

/**
 * Service d'ajout de zone economiques
 * @param {*} zoneData 
 * @returns 
 */
export default function addZoneEconomique(zoneData, token) {
  
  const header = {
    //8'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`,
  };
  const response = axios.post(addZoneEconomiqueUrl, zoneData, { headers: header});
  return response.then(
    (data) => response,
    console.log('Ajouté avec succès')
  ).catch((error) => error);
}