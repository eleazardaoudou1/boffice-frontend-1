import React from 'react';
import Header from '../Header';
import FlexColumn from '../FlexColumn';
import ToolBar from './ToolBar';

const Dashboard = () => {
    return (

        <div>
            <FlexColumn childComponent={<ToolBar />} />
        </div>

    );
};

export default Dashboard;