import React from 'react';
import Header from './Header';
import ToolBar from './Dashboard/ToolBar';

const FlexColumn = () => {
    const childComponent = <ToolBar />
    return (
        <div className="d-flex flex-column flex-root">
            <div className="page d-flex flex-row flex-column-fluid">
                <div className="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <Header />
                <ToolBar />
                </div>
            </div>
        </div>
    );
};

export default FlexColumn;