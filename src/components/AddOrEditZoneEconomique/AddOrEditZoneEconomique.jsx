import React, { useState } from 'react';
import addZoneEconomique from '../../services/ZoneEconomiqueService';

const AddOrEditZoneEconomique = () => {
    const [data, setData] = useState();
   
    /**
     * Gérer la soumission du formulaire
     * @param 
     */
    const handleSubmit = (e) => {
		e.preventDefault();

        console.log('Le token est : ')
        console.log(localStorage.getItem('token'))
        const token = localStorage.getItem('token');
       
        const  finalData = {"code" : data.code, "designation" : data.designation, "ou" : "ici", "quand" : "rfed"}
        console.log(finalData)
		addZoneEconomique(finalData, token)
	}

    return (
        <div>
            {/*<!--begin::Modal - Adjust Balance--> */}
            <div className="modal fade" id="kt_modal_export_users" tabIndex="-1" aria-hidden="true">
                {/*<!--begin::Modal dialog--> */}
                <div className="modal-dialog modal-dialog-centered mw-650px">
                    {/*<!--begin::Modal content--> */}
                    <div className="modal-content">
                        {/*<!--begin::Modal header--> */}
                        <div className="modal-header">
                            {/*<!--begin::Modal title--> */}
                            <h2 className="fw-bolder">Ajout Zone Economique</h2>
                            {/*<!--end::Modal title--> */}
                            {/*<!--begin::Close--> */}
                            <div className="btn btn-icon btn-sm btn-active-icon-primary" data-kt-users-modal-action="close">
                                {/*<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg--> */}
                                <span className="svg-icon svg-icon-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                        <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                    </svg>
                                </span>
                                {/*<!--end::Svg Icon--> */}
                            </div>
                            {/*<!--end::Close--> */}
                        </div>
                        {/*<!--end::Modal header--> */}
                        {/*<!--begin::Modal body--> */}
                        <div className="modal-body scroll-y mx-5 mx-xl-15 my-7">
                            {/*<!--begin::Form--> */}
                            <form id="kt_modal_export_users_form" className="form" action="#">
                                {/*<!--begin::Input group-->*/}
                                <div className="fv-row mb-7">
                                    {/*<!--begin::Label-->*/}
                                    <label className="required fw-bold fs-6 mb-2">Code</label>
                                    {/*<!--end::Label-->*/}
                                    {/*<!--begin::Input-->*/}
                                    <input type="text" name="user_name" className="form-control form-control-solid mb-3 mb-lg-0" placeholder="Code" onChange={(e) => { setData({ ...data, code: e.target.value }) }} />
                                    {/*<!--end::Input-->*/}
                                </div>
                                {/*<!--end::Input group-->*/}
                                {/*<!--begin::Input group-->*/}
                                <div className="fv-row mb-7">
                                    {/*<!--begin::Label-->*/}
                                    <label className="required fw-bold fs-6 mb-2">Désignation</label>
                                    {/*<!--end::Label-->*/}
                                    {/*<!--begin::Input-->*/}
                                    <input type="text" name="user_name" className="form-control form-control-solid mb-3 mb-lg-0" placeholder="Désignation" onChange={(e) => { setData({ ...data, designation: e.target.value }) }} />
                                    {/*<!--end::Input-->*/}
                                </div>
                                {/*<!--end::Input group-->*/}
                                {/*<!--begin::Actions--> */}
                                <div className="text-center">
                                    <button type="reset" className="btn btn-light me-3" data-kt-users-modal-action="cancel">Fermer</button>
                                    <button type="submit" className="btn btn-primary" data-kt-users-modal-action="submit" onClick={(e) => { handleSubmit(e) }}>
                                        <span className="indicator-label">Ajouter</span>
                                        <span className="indicator-progress">Please wait...
                                            <span className="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                </div>
                                {/*<!--end::Actions--> */}
                            </form>
                            {/*<!--end::Form--> */}
                        </div>
                        {/*<!--end::Modal body--> */}
                    </div>
                    {/*<!--end::Modal content--> */}
                </div>
                {/*<!--end::Modal dialog--> */}
            </div>
            {/*<!--end::Modal - New Card--> */}
        </div>
    );
};

export default AddOrEditZoneEconomique;