import React from 'react';

const Header = () => {
    return (
        <div id="kt_header" className="header">
            {/* {/*<!--begin::Header top--> */}
            <div className="header-top d-flex align-items-stretch flex-grow-1">
                {/* {/*<!--begin::Container--> */}
                <div className="d-flex container-xxl w-100">
                    {/* {/*<!--begin::Wrapper--> */}
                    <div className="d-flex flex-stack align-items-stretch w-100">
                        {/* {/*<!--begin::Brand--> */}
                        <div className="d-flex align-items-center align-items-lg-stretch me-5">
                            {/* {/*<!--begin::Heaeder navs toggle--> */}
                            <button className="d-lg-none btn btn-icon btn-color-white bg-hover-white bg-hover-opacity-10 w-35px h-35px h-md-40px w-md-40px ms-n2 me-2" id="kt_header_navs_toggle">
                                {/* {/*<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg--> */}
                                <span className="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
                                        <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
                                    </svg>
                                </span>
                                {/* {/*<!--end::Svg Icon--> */}
                            </button>
                            {/* {/*<!--end::Heaeder navs toggle--> */}
                            {/* {/*<!--begin::Logo--> */}
                            <a href="../../demo20/dist/index.html" className="d-flex align-items-center">
                                <img alt="Logo" src="assets/media/logos/logo-demo20.svg" className="h-25px h-lg-30px" />
                            </a>
                            {/* {/*<!--end::Logo--> */}
                            <div className="align-self-end" id="kt_brand_tabs">
                                {/* {/*<!--begin::Header tabs--> */}
                                <div className="header-tabs overflow-auto mx-4 ms-lg-10 mb-5 mb-lg-0" id="kt_header_tabs" data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_header_navs_wrapper', lg: '#kt_brand_tabs'}">
                                    <ul className="nav flex-nowrap">
                                        <li className="nav-item">
                                            <a className="nav-link active" data-bs-toggle="tab" href="#kt_header_navs_tab_1">Système</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-bs-toggle="tab" href="#kt_header_navs_tab_2">Mise en place</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-bs-toggle="tab" href="#kt_header_navs_tab_3">Tiers</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-bs-toggle="tab" href="#kt_header_navs_tab_4">Paramétrages</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-bs-toggle="tab" href="#kt_header_navs_tab_5">Produits & Stocks</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-bs-toggle="tab" href="#kt_header_navs_tab_6">Entrées</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-bs-toggle="tab" href="#kt_header_navs_tab_7">Sorties</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-bs-toggle="tab" href="#kt_header_navs_tab_8">Plus</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-bs-toggle="tab" href="#kt_header_navs_tab_9">Publipostage</a>
                                        </li>

                                    </ul>
                                </div>
                                {/* {/*<!--end::Header tabs--> */}
                            </div>
                        </div>
                        {/* {/*<!--end::Brand--> */}
                        {/* {/*<!--begin::Topbar--> */}
                        <div className="d-flex align-items-center flex-shrink-0">
                            {/* {/*<!--begin::Chat--> */}
                            <div className="d-flex align-items-center ms-1">
                                {/* {/*<!--begin::Menu wrapper--> */}
                                <div className="btn btn-icon btn-color-white bg-hover-white bg-hover-opacity-10 w-35px h-35px h-md-40px w-md-40px position-relative" id="kt_drawer_chat_toggle">
                                    {/* {/*<!--begin::Svg Icon | path: icons/duotune/communication/com012.svg--> */}
                                    <span className="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3" d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z" fill="black" />
                                            <rect x="6" y="12" width="7" height="2" rx="1" fill="black" />
                                            <rect x="6" y="7" width="12" height="2" rx="1" fill="black" />
                                        </svg>
                                    </span>
                                    {/* {/*<!--end::Svg Icon--> */}
                                    {/* {/*<!--begin::Notification animation--> */}
                                    <span className="bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-50 animation-blink"></span>
                                    {/* {/*<!--end::Notification animation--> */}
                                </div>
                                {/* {/*<!--end::Menu wrapper--> */}
                            </div>
                            {/* {/*<!--end::Chat--> */}

                            {/* {/*<!--begin::User--> */}
                            <div className="d-flex align-items-center ms-1" id="kt_header_user_menu_toggle">
                                {/* {/*<!--begin::User info--> */}
                                <div className="btn btn-flex align-items-center bg-hover-white bg-hover-opacity-10 py-2 px-2 px-md-3" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                                    {/* {/*<!--begin::Name--> */}
                                    <div className="d-none d-md-flex flex-column align-items-end justify-content-center me-2 me-md-4">
                                        <span className="text-white opacity-75 fs-8 fw-bold lh-1 mb-1">Max</span>
                                        <span className="text-white fs-8 fw-bolder lh-1">UX Designer</span>
                                    </div>
                                    {/* {/*<!--end::Name--> */}
                                    {/* {/*<!--begin::Symbol--> */}
                                    <div className="symbol symbol-30px symbol-md-40px">
                                        <img src="assets/media/avatars/300-1.jpg" alt="image" />
                                    </div>
                                    {/* {/*<!--end::Symbol--> */}
                                </div>
                                {/* {/*<!--end::User info--> */}
                                {/* {/*<!--begin::User account menu--> */}
                                <div className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px" data-kt-menu="true">
                                    {/* {/*<!--begin::Menu item--> */}
                                    <div className="menu-item px-3">
                                        <div className="menu-content d-flex align-items-center px-3">
                                            {/* {/*<!--begin::Avatar--> */}
                                            <div className="symbol symbol-50px me-5">
                                                <img alt="Logo" src="assets/media/avatars/300-1.jpg" />
                                            </div>
                                            {/* {/*<!--end::Avatar--> */}
                                            {/* {/*<!--begin::Username--> */}
                                            <div className="d-flex flex-column">
                                                <div className="fw-bolder d-flex align-items-center fs-5">Max Smith
                                                    <span className="badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2">Pro</span></div>
                                                <a href="#" className="fw-bold text-muted text-hover-primary fs-7">max@kt.com</a>
                                            </div>
                                            {/* {/*<!--end::Username--> */}
                                        </div>
                                    </div>
                                    {/* {/*<!--end::Menu item--> */}
                                    {/* {/*<!--begin::Menu separator--> */}
                                    <div className="separator my-2"></div>
                                    {/* {/*<!--end::Menu separator--> */}
                                    {/* {/*<!--begin::Menu item--> */}
                                    <div className="menu-item px-5">
                                        <a href="../../demo20/dist/account/overview.html" className="menu-link px-5">My Profile</a>
                                    </div>
                                    {/* {/*<!--end::Menu item--> */}
                                    {/* {/*<!--begin::Menu item--> */}
                                    <div className="menu-item px-5">
                                        <a href="../../demo20/dist/apps/projects/list.html" className="menu-link px-5">
                                            <span className="menu-text">My Projects</span>
                                            <span className="menu-badge">
                                                <span className="badge badge-light-danger badge-circle fw-bolder fs-7">3</span>
                                            </span>
                                        </a>
                                    </div>
                                    {/* {/*<!--end::Menu item--> */}
                                    {/* {/*<!--begin::Menu item--> */}
                                    <div className="menu-item px-5" data-kt-menu-trigger="hover" data-kt-menu-placement="left-start">
                                        <a href="#" className="menu-link px-5">
                                            <span className="menu-title">My Subscription</span>
                                            <span className="menu-arrow"></span>
                                        </a>
                                        {/* {/*<!--begin::Menu sub--> */}
                                        <div className="menu-sub menu-sub-dropdown w-175px py-4">
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <a href="../../demo20/dist/account/referrals.html" className="menu-link px-5">Referrals</a>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <a href="../../demo20/dist/account/billing.html" className="menu-link px-5">Billing</a>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <a href="../../demo20/dist/account/statements.html" className="menu-link px-5">Payments</a>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <a href="../../demo20/dist/account/statements.html" className="menu-link d-flex flex-stack px-5">Statements
                                                    <i className="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="View your statements"></i></a>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                            {/* {/*<!--begin::Menu separator--> */}
                                            <div className="separator my-2"></div>
                                            {/* {/*<!--end::Menu separator--> */}
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <div className="menu-content px-3">
                                                    <label className="form-check form-switch form-check-custom form-check-solid">
                                                        <input className="form-check-input w-30px h-20px" type="checkbox" value="1" defaultChecked={"checked"} name="notifications" />
                                                        <span className="form-check-label text-muted fs-7">Notifications</span>
                                                    </label>
                                                </div>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                        </div>
                                        {/* {/*<!--end::Menu sub--> */}
                                    </div>
                                    {/* {/*<!--end::Menu item--> */}
                                    {/* {/*<!--begin::Menu item--> */}
                                    <div className="menu-item px-5">
                                        <a href="../../demo20/dist/account/statements.html" className="menu-link px-5">My Statements</a>
                                    </div>
                                    {/* {/*<!--end::Menu item--> */}
                                    {/* {/*<!--begin::Menu separator--> */}
                                    <div className="separator my-2"></div>
                                    {/* {/*<!--end::Menu separator--> */}
                                    {/* {/*<!--begin::Menu item--> */}
                                    <div className="menu-item px-5" data-kt-menu-trigger="hover" data-kt-menu-placement="left-start">
                                        <a href="#" className="menu-link px-5">
                                            <span className="menu-title position-relative">Language
                                                <span className="fs-8 rounded bg-light px-3 py-2 position-absolute translate-middle-y top-50 end-0">English
                                                    <img className="w-15px h-15px rounded-1 ms-2" src="assets/media/flags/united-states.svg" alt="" /></span></span>
                                        </a>
                                        {/* {/*<!--begin::Menu sub--> */}
                                        <div className="menu-sub menu-sub-dropdown w-175px py-4">
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <a href="../../demo20/dist/account/settings.html" className="menu-link d-flex px-5 active">
                                                    <span className="symbol symbol-20px me-4">
                                                        <img className="rounded-1" src="assets/media/flags/united-states.svg" alt="" />
                                                    </span>English</a>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <a href="../../demo20/dist/account/settings.html" className="menu-link d-flex px-5">
                                                    <span className="symbol symbol-20px me-4">
                                                        <img className="rounded-1" src="assets/media/flags/spain.svg" alt="" />
                                                    </span>Spanish</a>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <a href="../../demo20/dist/account/settings.html" className="menu-link d-flex px-5">
                                                    <span className="symbol symbol-20px me-4">
                                                        <img className="rounded-1" src="assets/media/flags/germany.svg" alt="" />
                                                    </span>German</a>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <a href="../../demo20/dist/account/settings.html" className="menu-link d-flex px-5">
                                                    <span className="symbol symbol-20px me-4">
                                                        <img className="rounded-1" src="assets/media/flags/japan.svg" alt="" />
                                                    </span>Japanese</a>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                            {/* {/*<!--begin::Menu item--> */}
                                            <div className="menu-item px-3">
                                                <a href="../../demo20/dist/account/settings.html" className="menu-link d-flex px-5">
                                                    <span className="symbol symbol-20px me-4">
                                                        <img className="rounded-1" src="assets/media/flags/france.svg" alt="" />
                                                    </span>French</a>
                                            </div>
                                            {/* {/*<!--end::Menu item--> */}
                                        </div>
                                        {/* {/*<!--end::Menu sub--> */}
                                    </div>
                                    {/* {/*<!--end::Menu item--> */}
                                    {/* {/*<!--begin::Menu item--> */}
                                    <div className="menu-item px-5 my-1">
                                        <a href="../../demo20/dist/account/settings.html" className="menu-link px-5">Account Settings</a>
                                    </div>
                                    {/* {/*<!--end::Menu item--> */}
                                    {/* {/*<!--begin::Menu item--> */}
                                    <div className="menu-item px-5">
                                        <a href="../../demo20/dist/authentication/flows/basic/sign-in.html" className="menu-link px-5">Sign Out</a>
                                    </div>
                                    {/* {/*<!--end::Menu item--> */}
                                    {/* {/*<!--begin::Menu separator--> */}
                                    <div className="separator my-2"></div>
                                    {/* {/*<!--end::Menu separator--> */}
                                    {/* {/*<!--begin::Menu item--> */}
                                    <div className="menu-item px-5">
                                        <div className="menu-content px-5">
                                            <label className="form-check form-switch form-check-custom form-check-solid pulse pulse-success" htmlFor="kt_user_menu_dark_mode_toggle">
                                                <input className="form-check-input w-30px h-20px" type="checkbox" value="1" name="mode" id="kt_user_menu_dark_mode_toggle" data-kt-url="../../demo20/dist/index.html" />
                                                <span className="pulse-ring ms-n1"></span>
                                                <span className="form-check-label text-gray-600 fs-7">Dark Mode</span>
                                            </label>
                                        </div>
                                    </div>
                                    {/* {/*<!--end::Menu item--> */}
                                </div>
                                {/* {/*<!--end::User account menu--> */}
                            </div>
                            {/* {/*<!--end::User --> */}
                            {/* {/*<!--begin::Heaeder menu toggle--> */}
                            {/* {/*<!--end::Heaeder menu toggle--> */}
                        </div>
                        {/* {/*<!--end::Topbar--> */}
                    </div>
                    {/* {/*<!--end::Wrapper--> */}
                </div>
                {/* {/*<!--end::Container--> */}
            </div>
            {/* {/*<!--end::Header top--> */}
            {/* {/*<!--begin::Header navs--> */}
            <div className="header-navs d-flex align-items-stretch flex-stack h-lg-70px w-100 py-5 py-lg-0" id="kt_header_navs" data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_header_navs_toggle" data-kt-swapper="true" data-kt-swapper-mode="append" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header'}">
                {/* {/*<!--begin::Container--> */}
                <div className="d-lg-flex container-xxl w-100">
                    {/* {/*<!--begin::Wrapper--> */}
                    <div className="d-lg-flex flex-column justify-content-lg-center w-100" id="kt_header_navs_wrapper">
                        {/* {/*<!--begin::Header tab content--> */}
                        <div className="tab-content" data-kt-scroll="true" data-kt-scroll-activate="{default: true, lg: false}" data-kt-scroll-height="auto" data-kt-scroll-offset="70px">
                            {/* {/*<!--begin::Tab panel--> */}
                            <div className="tab-pane fade active show" id="kt_header_navs_tab_1">
                                {/* {/*<!--begin::Menu wrapper--> */}
                                <div className="header-menu flex-column align-items-stretch flex-lg-row">
                                    {/* {/*<!--begin::Menu--> */}
                                    <div className="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item here show menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Sécurité des données</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                                                <div className="menu-item">
                                                    <a className="menu-link active py-3" href="../../demo20/dist/index.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Profils</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/ecommerce.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Privilèges</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/store-analytics.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Utilisateurs</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/logistics.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Connexions en cours</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/delivery.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Connexions archivées</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/marketing.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">IHM(s)</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/social.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Paramétrage d'IHM(s) avec validation sans demande préalable</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Intégrité des données</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                                                <div className="menu-item">
                                                    <a className="menu-link active py-3" href="../../demo20/dist/index.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Sauvegarde de la Base de données</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/ecommerce.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Informations d'audit</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Messages</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                    </div>
                                    {/* {/*<!--end::Menu--> */}
                                </div>
                                {/* {/*<!--end::Menu wrapper--> */}
                            </div>
                            {/* {/*<!--end::Tab panel--> */}


                            <div className="tab-pane fade show" id="kt_header_navs_tab_2">
                                {/*<!--begin::Menu wrapper--> */}
                                <div className="header-menu flex-column align-items-stretch flex-lg-row">
                                    {/*<!--begin::Menu--> */}
                                    <div className="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">

                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Informations généralistes</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-255px">
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm007.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M21 9V11C21 11.6 20.6 12 20 12H14V8H20C20.6 8 21 8.4 21 9ZM10 8H4C3.4 8 3 8.4 3 9V11C3 11.6 3.4 12 4 12H10V8Z" fill="black" />
                                                                    <path d="M15 2C13.3 2 12 3.3 12 5V8H15C16.7 8 18 6.7 18 5C18 3.3 16.7 2 15 2Z" fill="black" />
                                                                    <path opacity="0.3" d="M9 2C10.7 2 12 3.3 12 5V8H9C7.3 8 6 6.7 6 5C6 3.3 7.3 2 9 2ZM4 12V21C4 21.6 4.4 22 5 22H10V12H4ZM20 12V21C20 21.6 19.6 22 19 22H14V12H20Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Pages</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                            <span className="menu-link py-3">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Profile</span>
                                                                <span className="menu-arrow"></span>
                                                            </span>
                                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/user-profile/overview.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Overview</span>
                                                                    </a>
                                                                </div>
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/user-profile/projects.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Projects</span>
                                                                    </a>
                                                                </div>
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/user-profile/campaigns.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Campaigns</span>
                                                                    </a>
                                                                </div>
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/user-profile/documents.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Documents</span>
                                                                    </a>
                                                                </div>
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/user-profile/followers.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Followers</span>
                                                                    </a>
                                                                </div>
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/user-profile/activity.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Activity</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                            <span className="menu-link py-3">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Blog</span>
                                                                <span className="menu-arrow"></span>
                                                            </span>
                                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/blog/home.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Blog Home</span>
                                                                    </a>
                                                                </div>
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/blog/post.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Blog Post</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                            <span className="menu-link py-3">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Pricing</span>
                                                                <span className="menu-arrow"></span>
                                                            </span>
                                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/pricing/pricing-1.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Pricing 1</span>
                                                                    </a>
                                                                </div>
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/pricing/pricing-2.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Pricing 2</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                            <span className="menu-link py-3">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Careers</span>
                                                                <span className="menu-arrow"></span>
                                                            </span>
                                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/careers/list.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Careers List</span>
                                                                    </a>
                                                                </div>
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/careers/apply.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Careers Apply</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                            <span className="menu-link py-3">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">FAQ</span>
                                                                <span className="menu-arrow"></span>
                                                            </span>
                                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/faq/classic.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Classic</span>
                                                                    </a>
                                                                </div>
                                                                <div className="menu-item">
                                                                    <a className="menu-link py-3" href="../../demo20/dist/pages/faq/extended.html">
                                                                        <span className="menu-bullet">
                                                                            <span className="bullet bullet-dot"></span>
                                                                        </span>
                                                                        <span className="menu-title">Extended</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/pages/about.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">About Us</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/pages/contact.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Contact Us</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/pages/team.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Our Team</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/pages/licenses.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Licenses</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/pages/sitemap.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Sitemap</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations géographiques</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Zones économiques</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Pays</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/security.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Départements territoriaux</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/billing.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Communes territoriales</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/statements.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Arrondissements territoriaux</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/referrals.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Villages ou quartiers de ville</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/api-keys.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Sites</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/logs.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Catégories de lieux d'affectation</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/logs.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Boutiques</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/logs.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Magasins</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/logs.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Rayons de rangements</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/logs.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Zones d'inventaires</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations financières</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Banques</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Devises</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/security.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Instruments de règlements</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/billing.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Modes de règlements</span>
                                                            </a>
                                                        </div>
                                                      
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations sur les produits</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Famille de produits</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Sous-familles de produits</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/security.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Catégories d'inventaires de produits</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/billing.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types d'activités</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/statements.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Fabricants</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/referrals.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Marques</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/api-keys.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Modèles</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/logs.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Emballages</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/logs.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types de coéfficients de frais accessoires</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations sur les tiers</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Familles d'acteurs</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Raisons de mises de fournisseurs sur liste noire</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/security.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Statuts juridiques</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/billing.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Segments d'activités</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/statements.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Secteurs d'activités</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/referrals.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Catégories de solvabilité des clients</span>
                                                            </a>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations commerciales</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types de réductions commerciales</span>
                                                            </a>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations de Logistique</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Modes de transports</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types d'étapes dans le suivi des commandes fournisseurs</span>
                                                            </a>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations fiscales</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types de taxes</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Régimes fiscaux</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/security.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types de retenues</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/billing.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Groupes de taxations</span>
                                                            </a>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Diverses autres informations</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Périodicités</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types de messages</span>
                                                            </a>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Informations de gestion commerciale</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/general/gen002.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M4.05424 15.1982C8.34524 7.76818 13.5782 3.26318 20.9282 2.01418C21.0729 1.98837 21.2216 1.99789 21.3618 2.04193C21.502 2.08597 21.6294 2.16323 21.7333 2.26712C21.8372 2.37101 21.9144 2.49846 21.9585 2.63863C22.0025 2.7788 22.012 2.92754 21.9862 3.07218C20.7372 10.4222 16.2322 15.6552 8.80224 19.9462L4.05424 15.1982ZM3.81924 17.3372L2.63324 20.4482C2.58427 20.5765 2.5735 20.7163 2.6022 20.8507C2.63091 20.9851 2.69788 21.1082 2.79503 21.2054C2.89218 21.3025 3.01536 21.3695 3.14972 21.3982C3.28408 21.4269 3.42387 21.4161 3.55224 21.3672L6.66524 20.1802L3.81924 17.3372ZM16.5002 5.99818C16.2036 5.99818 15.9136 6.08615 15.6669 6.25097C15.4202 6.41579 15.228 6.65006 15.1144 6.92415C15.0009 7.19824 14.9712 7.49984 15.0291 7.79081C15.0869 8.08178 15.2298 8.34906 15.4396 8.55884C15.6494 8.76862 15.9166 8.91148 16.2076 8.96935C16.4986 9.02723 16.8002 8.99753 17.0743 8.884C17.3484 8.77046 17.5826 8.5782 17.7474 8.33153C17.9123 8.08486 18.0002 7.79485 18.0002 7.49818C18.0002 7.10035 17.8422 6.71882 17.5609 6.43752C17.2796 6.15621 16.8981 5.99818 16.5002 5.99818Z" fill="black" />
                                                                    <path d="M4.05423 15.1982L2.24723 13.3912C2.15505 13.299 2.08547 13.1867 2.04395 13.0632C2.00243 12.9396 1.9901 12.8081 2.00793 12.679C2.02575 12.5498 2.07325 12.4266 2.14669 12.3189C2.22013 12.2112 2.31752 12.1219 2.43123 12.0582L9.15323 8.28918C7.17353 10.3717 5.4607 12.6926 4.05423 15.1982ZM8.80023 19.9442L10.6072 21.7512C10.6994 21.8434 10.8117 21.9129 10.9352 21.9545C11.0588 21.996 11.1903 22.0083 11.3195 21.9905C11.4486 21.9727 11.5718 21.9252 11.6795 21.8517C11.7872 21.7783 11.8765 21.6809 11.9402 21.5672L15.7092 14.8442C13.6269 16.8245 11.3061 18.5377 8.80023 19.9442ZM7.04023 18.1832L12.5832 12.6402C12.7381 12.4759 12.8228 12.2577 12.8195 12.032C12.8161 11.8063 12.725 11.5907 12.5653 11.4311C12.4057 11.2714 12.1901 11.1803 11.9644 11.1769C11.7387 11.1736 11.5205 11.2583 11.3562 11.4132L5.81323 16.9562L7.04023 18.1832Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations sur les produits</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/list.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Famille de produits pour les assurances</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/project.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Familles de produits pour la vente aux comptoirs</span>
                                                            </a>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/general/gen002.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M4.05424 15.1982C8.34524 7.76818 13.5782 3.26318 20.9282 2.01418C21.0729 1.98837 21.2216 1.99789 21.3618 2.04193C21.502 2.08597 21.6294 2.16323 21.7333 2.26712C21.8372 2.37101 21.9144 2.49846 21.9585 2.63863C22.0025 2.7788 22.012 2.92754 21.9862 3.07218C20.7372 10.4222 16.2322 15.6552 8.80224 19.9462L4.05424 15.1982ZM3.81924 17.3372L2.63324 20.4482C2.58427 20.5765 2.5735 20.7163 2.6022 20.8507C2.63091 20.9851 2.69788 21.1082 2.79503 21.2054C2.89218 21.3025 3.01536 21.3695 3.14972 21.3982C3.28408 21.4269 3.42387 21.4161 3.55224 21.3672L6.66524 20.1802L3.81924 17.3372ZM16.5002 5.99818C16.2036 5.99818 15.9136 6.08615 15.6669 6.25097C15.4202 6.41579 15.228 6.65006 15.1144 6.92415C15.0009 7.19824 14.9712 7.49984 15.0291 7.79081C15.0869 8.08178 15.2298 8.34906 15.4396 8.55884C15.6494 8.76862 15.9166 8.91148 16.2076 8.96935C16.4986 9.02723 16.8002 8.99753 17.0743 8.884C17.3484 8.77046 17.5826 8.5782 17.7474 8.33153C17.9123 8.08486 18.0002 7.79485 18.0002 7.49818C18.0002 7.10035 17.8422 6.71882 17.5609 6.43752C17.2796 6.15621 16.8981 5.99818 16.5002 5.99818Z" fill="black" />
                                                                    <path d="M4.05423 15.1982L2.24723 13.3912C2.15505 13.299 2.08547 13.1867 2.04395 13.0632C2.00243 12.9396 1.9901 12.8081 2.00793 12.679C2.02575 12.5498 2.07325 12.4266 2.14669 12.3189C2.22013 12.2112 2.31752 12.1219 2.43123 12.0582L9.15323 8.28918C7.17353 10.3717 5.4607 12.6926 4.05423 15.1982ZM8.80023 19.9442L10.6072 21.7512C10.6994 21.8434 10.8117 21.9129 10.9352 21.9545C11.0588 21.996 11.1903 22.0083 11.3195 21.9905C11.4486 21.9727 11.5718 21.9252 11.6795 21.8517C11.7872 21.7783 11.8765 21.6809 11.9402 21.5672L15.7092 14.8442C13.6269 16.8245 11.3061 18.5377 8.80023 19.9442ZM7.04023 18.1832L12.5832 12.6402C12.7381 12.4759 12.8228 12.2577 12.8195 12.032C12.8161 11.8063 12.725 11.5907 12.5653 11.4311C12.4057 11.2714 12.1901 11.1803 11.9644 11.1769C11.7387 11.1736 11.5205 11.2583 11.3562 11.4132L5.81323 16.9562L7.04023 18.1832Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations sur les tiers</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/list.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Groupes de clients pour vente aux comptoirs</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/project.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Raisons de mises de clients sur liste noire</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/general/gen002.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M4.05424 15.1982C8.34524 7.76818 13.5782 3.26318 20.9282 2.01418C21.0729 1.98837 21.2216 1.99789 21.3618 2.04193C21.502 2.08597 21.6294 2.16323 21.7333 2.26712C21.8372 2.37101 21.9144 2.49846 21.9585 2.63863C22.0025 2.7788 22.012 2.92754 21.9862 3.07218C20.7372 10.4222 16.2322 15.6552 8.80224 19.9462L4.05424 15.1982ZM3.81924 17.3372L2.63324 20.4482C2.58427 20.5765 2.5735 20.7163 2.6022 20.8507C2.63091 20.9851 2.69788 21.1082 2.79503 21.2054C2.89218 21.3025 3.01536 21.3695 3.14972 21.3982C3.28408 21.4269 3.42387 21.4161 3.55224 21.3672L6.66524 20.1802L3.81924 17.3372ZM16.5002 5.99818C16.2036 5.99818 15.9136 6.08615 15.6669 6.25097C15.4202 6.41579 15.228 6.65006 15.1144 6.92415C15.0009 7.19824 14.9712 7.49984 15.0291 7.79081C15.0869 8.08178 15.2298 8.34906 15.4396 8.55884C15.6494 8.76862 15.9166 8.91148 16.2076 8.96935C16.4986 9.02723 16.8002 8.99753 17.0743 8.884C17.3484 8.77046 17.5826 8.5782 17.7474 8.33153C17.9123 8.08486 18.0002 7.79485 18.0002 7.49818C18.0002 7.10035 17.8422 6.71882 17.5609 6.43752C17.2796 6.15621 16.8981 5.99818 16.5002 5.99818Z" fill="black" />
                                                                    <path d="M4.05423 15.1982L2.24723 13.3912C2.15505 13.299 2.08547 13.1867 2.04395 13.0632C2.00243 12.9396 1.9901 12.8081 2.00793 12.679C2.02575 12.5498 2.07325 12.4266 2.14669 12.3189C2.22013 12.2112 2.31752 12.1219 2.43123 12.0582L9.15323 8.28918C7.17353 10.3717 5.4607 12.6926 4.05423 15.1982ZM8.80023 19.9442L10.6072 21.7512C10.6994 21.8434 10.8117 21.9129 10.9352 21.9545C11.0588 21.996 11.1903 22.0083 11.3195 21.9905C11.4486 21.9727 11.5718 21.9252 11.6795 21.8517C11.7872 21.7783 11.8765 21.6809 11.9402 21.5672L15.7092 14.8442C13.6269 16.8245 11.3061 18.5377 8.80023 19.9442ZM7.04023 18.1832L12.5832 12.6402C12.7381 12.4759 12.8228 12.2577 12.8195 12.032C12.8161 11.8063 12.725 11.5907 12.5653 11.4311C12.4057 11.2714 12.1901 11.1803 11.9644 11.1769C11.7387 11.1736 11.5205 11.2583 11.3562 11.4132L5.81323 16.9562L7.04023 18.1832Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations financières</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/list.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Coupures</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/project.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types de factures</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/project.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types de ventes aux comptoirs</span>
                                                            </a>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/general/gen002.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M4.05424 15.1982C8.34524 7.76818 13.5782 3.26318 20.9282 2.01418C21.0729 1.98837 21.2216 1.99789 21.3618 2.04193C21.502 2.08597 21.6294 2.16323 21.7333 2.26712C21.8372 2.37101 21.9144 2.49846 21.9585 2.63863C22.0025 2.7788 22.012 2.92754 21.9862 3.07218C20.7372 10.4222 16.2322 15.6552 8.80224 19.9462L4.05424 15.1982ZM3.81924 17.3372L2.63324 20.4482C2.58427 20.5765 2.5735 20.7163 2.6022 20.8507C2.63091 20.9851 2.69788 21.1082 2.79503 21.2054C2.89218 21.3025 3.01536 21.3695 3.14972 21.3982C3.28408 21.4269 3.42387 21.4161 3.55224 21.3672L6.66524 20.1802L3.81924 17.3372ZM16.5002 5.99818C16.2036 5.99818 15.9136 6.08615 15.6669 6.25097C15.4202 6.41579 15.228 6.65006 15.1144 6.92415C15.0009 7.19824 14.9712 7.49984 15.0291 7.79081C15.0869 8.08178 15.2298 8.34906 15.4396 8.55884C15.6494 8.76862 15.9166 8.91148 16.2076 8.96935C16.4986 9.02723 16.8002 8.99753 17.0743 8.884C17.3484 8.77046 17.5826 8.5782 17.7474 8.33153C17.9123 8.08486 18.0002 7.79485 18.0002 7.49818C18.0002 7.10035 17.8422 6.71882 17.5609 6.43752C17.2796 6.15621 16.8981 5.99818 16.5002 5.99818Z" fill="black" />
                                                                    <path d="M4.05423 15.1982L2.24723 13.3912C2.15505 13.299 2.08547 13.1867 2.04395 13.0632C2.00243 12.9396 1.9901 12.8081 2.00793 12.679C2.02575 12.5498 2.07325 12.4266 2.14669 12.3189C2.22013 12.2112 2.31752 12.1219 2.43123 12.0582L9.15323 8.28918C7.17353 10.3717 5.4607 12.6926 4.05423 15.1982ZM8.80023 19.9442L10.6072 21.7512C10.6994 21.8434 10.8117 21.9129 10.9352 21.9545C11.0588 21.996 11.1903 22.0083 11.3195 21.9905C11.4486 21.9727 11.5718 21.9252 11.6795 21.8517C11.7872 21.7783 11.8765 21.6809 11.9402 21.5672L15.7092 14.8442C13.6269 16.8245 11.3061 18.5377 8.80023 19.9442ZM7.04023 18.1832L12.5832 12.6402C12.7381 12.4759 12.8228 12.2577 12.8195 12.032C12.8161 11.8063 12.725 11.5907 12.5653 11.4311C12.4057 11.2714 12.1901 11.1803 11.9644 11.1769C11.7387 11.1736 11.5205 11.2583 11.3562 11.4132L5.81323 16.9562L7.04023 18.1832Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Informations commerciales</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/list.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types de garanties</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/project.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Types de marchés</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/apps/projects/project.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Actions clôturant projets</span>
                                                            </a>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item here show menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Informations de gestion de trésorerie</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                                                <div className="menu-item">
                                                    <a className="menu-link active py-3" href="../../demo20/dist/index.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Rubriques de dépenses et de recettes</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/ecommerce.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Caisses de menues dépenses et recettes</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/store-analytics.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Natures d'opérations de trésorerie</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/logistics.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Comptes de trésorerie</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/delivery.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Types de cautions</span>
                                                    </a>
                                                </div>
                                               
                                            </div>
                                        </div>

                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="top-start" className="menu-item menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Informations de gestion de patrimoine</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="scroll  menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-255px">
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/base/utilities.html" title="Check out over 200 in-house components, plugins and ready for use solutions" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: /icons/duotune/general/gen002.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M4.05424 15.1982C8.34524 7.76818 13.5782 3.26318 20.9282 2.01418C21.0729 1.98837 21.2216 1.99789 21.3618 2.04193C21.502 2.08597 21.6294 2.16323 21.7333 2.26712C21.8372 2.37101 21.9144 2.49846 21.9585 2.63863C22.0025 2.7788 22.012 2.92754 21.9862 3.07218C20.7372 10.4222 16.2322 15.6552 8.80224 19.9462L4.05424 15.1982ZM3.81924 17.3372L2.63324 20.4482C2.58427 20.5765 2.5735 20.7163 2.6022 20.8507C2.63091 20.9851 2.69788 21.1082 2.79503 21.2054C2.89218 21.3025 3.01536 21.3695 3.14972 21.3982C3.28408 21.4269 3.42387 21.4161 3.55224 21.3672L6.66524 20.1802L3.81924 17.3372ZM16.5002 5.99818C16.2036 5.99818 15.9136 6.08615 15.6669 6.25097C15.4202 6.41579 15.228 6.65006 15.1144 6.92415C15.0009 7.19824 14.9712 7.49984 15.0291 7.79081C15.0869 8.08178 15.2298 8.34906 15.4396 8.55884C15.6494 8.76862 15.9166 8.91148 16.2076 8.96935C16.4986 9.02723 16.8002 8.99753 17.0743 8.884C17.3484 8.77046 17.5826 8.5782 17.7474 8.33153C17.9123 8.08486 18.0002 7.79485 18.0002 7.49818C18.0002 7.10035 17.8422 6.71882 17.5609 6.43752C17.2796 6.15621 16.8981 5.99818 16.5002 5.99818Z" fill="black" />
                                                                    <path d="M4.05423 15.1982L2.24723 13.3912C2.15505 13.299 2.08547 13.1867 2.04395 13.0632C2.00243 12.9396 1.9901 12.8081 2.00793 12.679C2.02575 12.5498 2.07325 12.4266 2.14669 12.3189C2.22013 12.2112 2.31752 12.1219 2.43123 12.0582L9.15323 8.28918C7.17353 10.3717 5.4607 12.6926 4.05423 15.1982ZM8.80023 19.9442L10.6072 21.7512C10.6994 21.8434 10.8117 21.9129 10.9352 21.9545C11.0588 21.996 11.1903 22.0083 11.3195 21.9905C11.4486 21.9727 11.5718 21.9252 11.6795 21.8517C11.7872 21.7783 11.8765 21.6809 11.9402 21.5672L15.7092 14.8442C13.6269 16.8245 11.3061 18.5377 8.80023 19.9442ZM7.04023 18.1832L12.5832 12.6402C12.7381 12.4759 12.8228 12.2577 12.8195 12.032C12.8161 11.8063 12.725 11.5907 12.5653 11.4311C12.4057 11.2714 12.1901 11.1803 11.9644 11.1769C11.7387 11.1736 11.5205 11.2583 11.3562 11.4132L5.81323 16.9562L7.04023 18.1832Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types de biens</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="https://preview.keenthemes.com/metronic8/demo20/layout-builder.html" title="Build your layout, preview and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/general/gen019.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z" fill="black" />
                                                                    <path opacity="0.3" d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Natures comptables de biens</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started.html" title="Check out the complete documentation" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/abstract/abs027.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black" />
                                                                    <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types de contrôles techniques</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types d'opérations d'entretiens</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types d'infractions</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types causes incidents</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types d'incidents</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types de constituants de biens</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types de dommages aux biens</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types de garanties</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types de garanties d'assurances</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types d'énergies</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Localisations</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="top-start" className="menu-item menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Informations de gestion de comptabilité des engagements</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="scroll  menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-255px">
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/base/utilities.html" title="Check out over 200 in-house components, plugins and ready for use solutions" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: /icons/duotune/general/gen002.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M4.05424 15.1982C8.34524 7.76818 13.5782 3.26318 20.9282 2.01418C21.0729 1.98837 21.2216 1.99789 21.3618 2.04193C21.502 2.08597 21.6294 2.16323 21.7333 2.26712C21.8372 2.37101 21.9144 2.49846 21.9585 2.63863C22.0025 2.7788 22.012 2.92754 21.9862 3.07218C20.7372 10.4222 16.2322 15.6552 8.80224 19.9462L4.05424 15.1982ZM3.81924 17.3372L2.63324 20.4482C2.58427 20.5765 2.5735 20.7163 2.6022 20.8507C2.63091 20.9851 2.69788 21.1082 2.79503 21.2054C2.89218 21.3025 3.01536 21.3695 3.14972 21.3982C3.28408 21.4269 3.42387 21.4161 3.55224 21.3672L6.66524 20.1802L3.81924 17.3372ZM16.5002 5.99818C16.2036 5.99818 15.9136 6.08615 15.6669 6.25097C15.4202 6.41579 15.228 6.65006 15.1144 6.92415C15.0009 7.19824 14.9712 7.49984 15.0291 7.79081C15.0869 8.08178 15.2298 8.34906 15.4396 8.55884C15.6494 8.76862 15.9166 8.91148 16.2076 8.96935C16.4986 9.02723 16.8002 8.99753 17.0743 8.884C17.3484 8.77046 17.5826 8.5782 17.7474 8.33153C17.9123 8.08486 18.0002 7.79485 18.0002 7.49818C18.0002 7.10035 17.8422 6.71882 17.5609 6.43752C17.2796 6.15621 16.8981 5.99818 16.5002 5.99818Z" fill="black" />
                                                                    <path d="M4.05423 15.1982L2.24723 13.3912C2.15505 13.299 2.08547 13.1867 2.04395 13.0632C2.00243 12.9396 1.9901 12.8081 2.00793 12.679C2.02575 12.5498 2.07325 12.4266 2.14669 12.3189C2.22013 12.2112 2.31752 12.1219 2.43123 12.0582L9.15323 8.28918C7.17353 10.3717 5.4607 12.6926 4.05423 15.1982ZM8.80023 19.9442L10.6072 21.7512C10.6994 21.8434 10.8117 21.9129 10.9352 21.9545C11.0588 21.996 11.1903 22.0083 11.3195 21.9905C11.4486 21.9727 11.5718 21.9252 11.6795 21.8517C11.7872 21.7783 11.8765 21.6809 11.9402 21.5672L15.7092 14.8442C13.6269 16.8245 11.3061 18.5377 8.80023 19.9442ZM7.04023 18.1832L12.5832 12.6402C12.7381 12.4759 12.8228 12.2577 12.8195 12.032C12.8161 11.8063 12.725 11.5907 12.5653 11.4311C12.4057 11.2714 12.1901 11.1803 11.9644 11.1769C11.7387 11.1736 11.5205 11.2583 11.3562 11.4132L5.81323 16.9562L7.04023 18.1832Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types de branches budgétaires</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="https://preview.keenthemes.com/metronic8/demo20/layout-builder.html" title="Build your layout, preview and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/general/gen019.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z" fill="black" />
                                                                    <path opacity="0.3" d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Branches budgétaires </span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started.html" title="Check out the complete documentation" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/abstract/abs027.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black" />
                                                                    <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Types de budgets</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Chapitres budgétaires</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Articles budgétaires</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/documentation/getting-started/changelog.html">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/coding/cod003.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M16.95 18.9688C16.75 18.9688 16.55 18.8688 16.35 18.7688C15.85 18.4688 15.75 17.8688 16.05 17.3688L19.65 11.9688L16.05 6.56876C15.75 6.06876 15.85 5.46873 16.35 5.16873C16.85 4.86873 17.45 4.96878 17.75 5.46878L21.75 11.4688C21.95 11.7688 21.95 12.2688 21.75 12.5688L17.75 18.5688C17.55 18.7688 17.25 18.9688 16.95 18.9688ZM7.55001 18.7688C8.05001 18.4688 8.15 17.8688 7.85 17.3688L4.25001 11.9688L7.85 6.56876C8.15 6.06876 8.05001 5.46873 7.55001 5.16873C7.05001 4.86873 6.45 4.96878 6.15 5.46878L2.15 11.4688C1.95 11.7688 1.95 12.2688 2.15 12.5688L6.15 18.5688C6.35 18.8688 6.65 18.9688 6.95 18.9688C7.15 18.9688 7.35001 18.8688 7.55001 18.7688Z" fill="black" />
                                                                    <path opacity="0.3" d="M10.45 18.9687C10.35 18.9687 10.25 18.9687 10.25 18.9687C9.75 18.8687 9.35 18.2688 9.55 17.7688L12.55 5.76878C12.65 5.26878 13.25 4.8687 13.75 5.0687C14.25 5.1687 14.65 5.76878 14.45 6.26878L11.45 18.2688C11.35 18.6688 10.85 18.9687 10.45 18.9687Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Postes d'imputations budgétaires</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/*<!--end::Menu--> */}
                                </div>
                                {/*<!--end::Menu wrapper--> */}
                            </div>
                            <div className="tab-pane fade show" id="kt_header_navs_tab_3">
                                {/*<!--begin::Menu wrapper--> */}
                                <div className="header-menu flex-column align-items-stretch flex-lg-row">
                                    {/*<!--begin::Menu--> */}
                                    <div className="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item here show menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Tiers communs à tous les modules</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                                                <div className="menu-item">
                                                    <a className="menu-link active py-3" href="../../demo20/dist/index.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Acteurs</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/ecommerce.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Fournisseurs</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/store-analytics.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Lieux d'affectations</span>
                                                    </a>
                                                </div>
                                               
                                               
                                            </div>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item here show menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Tiers spécifiques à la gestion commerciale</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                                                <div className="menu-item">
                                                    <a className="menu-link active py-3" href="../../demo20/dist/index.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Clients</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/ecommerce.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Facilitateurs</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item here show menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Tiers spécifiques à la gestion de RH et paie</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                                                <div className="menu-item">
                                                    <a className="menu-link active py-3" href="../../demo20/dist/index.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Organismes de formation</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/ecommerce.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Organismes sociaux</span>
                                                    </a>
                                                </div>
                                                <div className="menu-item">
                                                    <a className="menu-link py-3" href="../../demo20/dist/dashboards/ecommerce.html">
                                                        <span className="menu-bullet">
                                                            <span className="bullet bullet-dot"></span>
                                                        </span>
                                                        <span className="menu-title">Employés</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>


                                       
                                    </div>
                                    {/*<!--end::Menu--> */}
                                </div>
                                {/*<!--end::Menu wrapper--> */}
                            </div>

                           
                            {/* {/*<!--begin::Tab panel--> */}
                            <div className="tab-pane fade" id="kt_header_navs_tab_4">
                                {/* {/*<!--begin::Wrapper--> */}
                                <div className="d-flex flex-column flex-lg-row flex-lg-stack flex-wrap gap-2 px-4 px-lg-0">
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-primary fw-bolder" href="../../demo20/dist/apps/ecommerce/catalog/products.html">eCommerce</a>
                                        <a className="btn btn-sm btn-light-danger fw-bolder" href="../../demo20/dist/apps/file-manager/folders.html">File Manager</a>
                                    </div>
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-info fw-bolder" href="../../demo20/dist/apps/subscriptions/view.html">More Apps</a>
                                    </div>
                                </div>
                                {/* {/*<!--end::Wrapper--> */}
                            </div>
                            {/* {/*<!--end::Tab panel--> */}

                            <div className="tab-pane fade show" id="kt_header_navs_tab_5">
                                {/*<!--begin::Menu wrapper--> */}
                                <div className="header-menu flex-column align-items-stretch flex-lg-row">
                                    {/*<!--begin::Menu--> */}
                                    <div className="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                    <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Produits</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Inventaires</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Stocks sites</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Stocks magasins</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Stocks boutiques</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Suggestions de réapprovisionnements</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Mouvements</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="top-start" className="menu-item menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Plus ...</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-255px">
                                              
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Situations momentanées de stocks</span>
                                                    </span>
                                                </div>

                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Transferts</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-225px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Transferts entre magasins</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Transferts magasins vers boutiques</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Transferts boutiques vers magasins</span>
                                                            </a>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Avariés</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Périmés</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Fiches de mouvements</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Entrées / Sorties n'ayant pas généré de mouvements</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Valorisation de stocks de produits (Inventaire et stock ad'hoc)</span>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    {/*<!--end::Menu--> */}
                                </div>
                                {/*<!--end::Menu wrapper--> */}
                            </div>
                            <div className="tab-pane fade show" id="kt_header_navs_tab_6">
                                {/*<!--begin::Menu wrapper--> */}
                                <div className="header-menu flex-column align-items-stretch flex-lg-row">
                                    {/*<!--begin::Menu--> */}
                                    <div className="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                    <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Commandes</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                    
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Dossiers d'importations</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Contrats d'abonnements</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Avances et acomptes payés sur commande</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Livraisons de commandes</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" className="menu-item  menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Facturations de commandes</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                        </div>
                                      
                                        
                                        <div data-kt-menu-trigger="click" data-kt-menu-placement="top-start" className="menu-item menu-lg-down-accordion me-lg-1">
                                            <span className="menu-link py-3">
                                                <span className="menu-title">Plus...</span>
                                                <span className="menu-arrow d-lg-none"></span>
                                            </span>
                                            <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-255px">
                                              
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Règlements de factures</span>
                                                    </span>
                                                </div>

                                               
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Liste noire fournisseurs</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Suivi détaillé des commandes</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Suivi global des commandes</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Tracking des commandes fournisseurs</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-255px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Mise en oeuvre de l'éligibilité de commandes fournisseurs au tracking</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Mise en oeuvre de la disponibilité des commandes fournisseurs élues au tracking</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Mise en oeuvre des enlèvements sur commandes fournisseurs élues au tracking</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Mise en oeuvre des expéditions sur commandes fournisseurs élues au tracking</span>
                                                            </a>
                                                        </div>
                                                      
                                                    
                                                    </div>
                                                </div>

                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Retours sur achats</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Avoirs reçus sur factures</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Actes de donations (dons reçus)</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Assemblage de produits (production)</span>
                                                        <span className="menu-arrow"></span>
                                                    </span>
                                                    <div className="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg py-lg-4 w-lg-255px">
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/overview.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Programme d'assemblage</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Assemblages réalisés</span>
                                                            </a>
                                                        </div>
                                                        <div className="menu-item">
                                                            <a className="menu-link py-3" href="../../demo20/dist/account/settings.html">
                                                                <span className="menu-bullet">
                                                                    <span className="bullet bullet-dot"></span>
                                                                </span>
                                                                <span className="menu-title">Assemblages en attente de réalisation</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Commandes fournisseurs en attente de livraisons</span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Commandes fournisseurs en attente de facturations </span>
                                                    </span>
                                                </div>
                                                <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" className="menu-item menu-lg-down-accordion">
                                                    <span className="menu-link py-3">
                                                        <span className="menu-icon">
                                                            {/*<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg--> */}
                                                            <span className="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                                                </svg>
                                                            </span>
                                                            {/*<!--end::Svg Icon--> */}
                                                        </span>
                                                        <span className="menu-title">Factures fournisseurs en attente de règlements</span>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    {/*<!--end::Menu--> */}
                                </div>
                                {/*<!--end::Menu wrapper--> */}
                            </div>

                           
                            {/* {/*<!--begin::Tab panel--> */}
                            <div className="tab-pane fade" id="kt_header_navs_tab_66">
                                {/* {/*<!--begin::Wrapper--> */}
                                <div className="d-flex flex-column flex-lg-row flex-lg-stack flex-wrap gap-2 px-4 px-lg-0">
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-primary fw-bolder" href="../../demo20/dist/documentation/getting-started.html">Documentation</a>
                                        <a className="btn btn-sm btn-light-success fw-bolder" href="../../demo20/dist/documentation/getting-started/video-tutorials.html">Video Tutorials</a>
                                        <a className="btn btn-sm btn-light-danger fw-bolder" href="https://preview.keenthemes.com/metronic8/demo20/layout-builder.html">Layout Builder</a>
                                    </div>
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-info fw-bolder" href="../../demo20/dist/documentation/getting-started/changelog.html">Changelog</a>
                                    </div>
                                </div>
                                {/* {/*<!--end::Wrapper--> */}
                            </div>
                            {/* {/*<!--end::Tab panel--> */}
                            {/* {/*<!--begin::Tab panel--> */}
                            <div className="tab-pane fade" id="kt_header_navs_tab_7">
                                {/* {/*<!--begin::Wrapper--> */}
                                <div className="d-flex flex-column flex-lg-row flex-lg-stack flex-wrap gap-2 px-4 px-lg-0">
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-primary fw-bolder" href="../../demo20/dist/documentation/getting-started.html">Documentation</a>
                                        <a className="btn btn-sm btn-light-success fw-bolder" href="../../demo20/dist/documentation/getting-started/video-tutorials.html">Video Tutorials</a>
                                        <a className="btn btn-sm btn-light-danger fw-bolder" href="https://preview.keenthemes.com/metronic8/demo20/layout-builder.html">Layout Builder</a>
                                    </div>
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-info fw-bolder" href="../../demo20/dist/documentation/getting-started/changelog.html">Changelog</a>
                                    </div>
                                </div>
                                {/* {/*<!--end::Wrapper--> */}
                            </div>
                            {/* {/*<!--end::Tab panel--> */}
                            {/* {/*<!--begin::Tab panel--> */}
                            <div className="tab-pane fade" id="kt_header_navs_tab_8">
                                {/* {/*<!--begin::Wrapper--> */}
                                <div className="d-flex flex-column flex-lg-row flex-lg-stack flex-wrap gap-2 px-4 px-lg-0">
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-primary fw-bolder" href="../../demo20/dist/documentation/getting-started.html">Documentation</a>
                                        <a className="btn btn-sm btn-light-success fw-bolder" href="../../demo20/dist/documentation/getting-started/video-tutorials.html">Video Tutorials</a>
                                        <a className="btn btn-sm btn-light-danger fw-bolder" href="https://preview.keenthemes.com/metronic8/demo20/layout-builder.html">Layout Builder</a>
                                    </div>
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-info fw-bolder" href="../../demo20/dist/documentation/getting-started/changelog.html">Changelog</a>
                                    </div>
                                </div>
                                {/* {/*<!--end::Wrapper--> */}
                            </div>
                            {/* {/*<!--end::Tab panel--> */}
                            {/* {/*<!--begin::Tab panel--> */}
                            <div className="tab-pane fade" id="kt_header_navs_tab_9">
                                {/* {/*<!--begin::Wrapper--> */}
                                <div className="d-flex flex-column flex-lg-row flex-lg-stack flex-wrap gap-2 px-4 px-lg-0">
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-primary fw-bolder" href="../../demo20/dist/documentation/getting-started.html">Documentation</a>
                                        <a className="btn btn-sm btn-light-success fw-bolder" href="../../demo20/dist/documentation/getting-started/video-tutorials.html">Video Tutorials</a>
                                        <a className="btn btn-sm btn-light-danger fw-bolder" href="https://preview.keenthemes.com/metronic8/demo20/layout-builder.html">Layout Builder</a>
                                    </div>
                                    <div className="d-flex flex-column flex-lg-row gap-2">
                                        <a className="btn btn-sm btn-light-info fw-bolder" href="../../demo20/dist/documentation/getting-started/changelog.html">Changelog</a>
                                    </div>
                                </div>
                                {/* {/*<!--end::Wrapper--> */}
                            </div>
                            {/* {/*<!--end::Tab panel--> */}
                        </div>
                        {/* {/*<!--end::Header tab content--> */}
                    </div>
                    {/* {/*<!--end::Wrapper--> */}
                </div>
                {/* {/*<!--end::Container--> */}
            </div>
            {/* {/*<!--end::Header navs--> */}
        </div>
    );
};

export default Header;